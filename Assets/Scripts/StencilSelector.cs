﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System;

public class StencilSelector : MonoBehaviour {
    MeshFilter meshFilter;

    enum floorTiles {carpet_A_A_01,
                     carpet_A_B_01,
                     carpet_A_C_01,
                     floor_A_A_01,
                     floor_A_B_01,
                     floor_A_C_01,
                     floor_B_A_01,
                     floor_B_B_01,
                     floor_B_C_01,
                     floor_C_A_01,
                     floor_C_B_01,
                     floor_C_C_01,
                     floor_D_A_01,
                     floor_D_A_423,
                     floor_D_B_01,
                     floor_D_C_01,
                     floor_E_A_01,
                     floor_E_B_01,
                     floor_E_B_02,
                     floor_F_A_01,
                     floor_F_B_01,
                     floor_F_C_01,
                     floor_qwdE_A_02,
                     floorq_Q_A_01,
                     floorq_Q_B_01,
                     floorq_Q_C_01,
                     floorq_Q_D_01
                     };

    public string floorTilesPath;

    public static int floorSelected = 0;
    public static string floorTileName = "";

    void Start () {
        meshFilter = GetComponentInChildren<MeshFilter>();
        changeFloorBrush();

	}

    public void changeFloorBrush()
    {
        //Debug.Log("I was changed as a floor forever");
        floorTileName = ((floorTiles)floorSelected).ToString();
        Mesh brushMesh = (Mesh)Resources.Load(floorTilesPath + floorTileName, typeof(Mesh));

        if (meshFilter == null)
            meshFilter = GetComponentInChildren<MeshFilter>();
        meshFilter.mesh = brushMesh;
    }

	// Update is called once per frame
	void Update () {
        float wheel = Input.GetAxis("Mouse ScrollWheel");
        if (wheel > 0f)
        {
            // scroll up
            if (floorSelected < System.Enum.GetValues(typeof(floorTiles)).Length - 1)
            floorSelected += 1;
            changeFloorBrush();

        }
        else if (wheel < 0f)
        {
            // scroll down
            if (floorSelected > 0)
                floorSelected -= 1;
            
            changeFloorBrush();
        }
	}
}
