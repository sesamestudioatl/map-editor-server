﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using System;

public class FloorData : MonoBehaviour
{
    Dictionary<string, string> tiles;
    public GameObject networkStencil;

    public SocketIOComponent socket;
    public bool useServer = true;

    [Serializable]
    class Tile
    {
        public Vector3 position;
        public string prefabName;
        public Tile(Vector3 location, string prefabName)
        {
            this.position = location;
            this.prefabName = prefabName;
        }

    }

    void Start()
    {
        tiles = new Dictionary<string, string>();
        SubscribeToServerEvents();
    }

    void SubscribeToServerEvents()
    {
        socket.On("tileSpawn", networkTileSpawn);
    }


    void networkTileSpawn(SocketIOEvent e)
    {
        //Debug.Log(string.Format("[name: {0}, data: {1}]", e.name, e.data));
        Tile tile = JsonUtility.FromJson<Tile>(e.data.ToString());

        //Debug.Log("Spawn: " + FoxHelper.print(tile.position));

        GameObject obj = (GameObject)Instantiate(networkStencil, tile.position, Quaternion.identity, transform);
        obj.name = tile.prefabName;
        obj.GetComponent<StencilSelector>().changeFloorBrush();
        Destroy(obj.GetComponent<StencilSelector>());
    }

    public void setTile(Vector3 location, GameObject stencil, bool spawnLocally, Transform parent)
    {
        string prefabName = StencilSelector.floorTileName;
        string key = FoxHelper.print(location);
        if (tiles.ContainsKey(key))
        {
            Debug.Log("TODO: Tile exists. Delete and spawn prefab");
            tiles[key] = prefabName;
        }
        else
        {
            Tile tile = new Tile(location, prefabName);

            tiles[key] = prefabName;
            if (useServer)
                socket.Emit("tileSpawn", new JSONObject(JsonUtility.ToJson(tile)));


            if (spawnLocally)
            {
                GameObject obj = (GameObject)Instantiate(stencil, location, stencil.transform.rotation, parent);
                obj.name = stencil.name;
                obj.GetComponent<StencilSelector>().changeFloorBrush();
                Destroy(obj.GetComponent<StencilSelector>());
            }
        }



    }

}
