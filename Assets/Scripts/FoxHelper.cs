﻿using UnityEngine;

public class FoxHelper {
    public static string print(Vector3 vec3)
    {
        return Mathf.Round(vec3.x).ToString() + "," + Mathf.Round(vec3.y).ToString() + "," + Mathf.Round(vec3.z).ToString();
    }

    public static Vector3 round(Vector3 vec)
    {
        Vector3 spawnPoint;
        spawnPoint.x = Mathf.Round(vec.x);
        spawnPoint.y = Mathf.Round(vec.y);
        spawnPoint.z = Mathf.Round(vec.z);

        return spawnPoint;
    }

    public static int roundToNearestTen(int x)
    {
        return (int)Mathf.Ceil((x-5)/ 10.0f) * 10;
    }
        
}
