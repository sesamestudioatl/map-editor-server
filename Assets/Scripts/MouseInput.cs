﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInput : MonoBehaviour {
    public GameObject stencil;
    public Transform parent;

    public FloorData floorData;
    public DictationScript dictScript;
    string lastVoiceCommand = "";

    public bool stencilToggle = true;
    public bool spawnLocally = false;
    public bool voiceControl = true;

    bool mouseDown = false;

    void Start()
    {
        if (dictScript == null)
            voiceControl = false;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            mouseDown = true;
        if (Input.GetMouseButtonUp(0))
            mouseDown = false;

        //apply brush stroke on left drag
        if (mouseDown)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                //Debug.Log("You selected the " + hit.transform.name); // ensure you picked right object
                //Debug.Log("Hit at: " + FoxHelper.print(hit.point));
                Vector3 spawnPoint = FoxHelper.round(hit.point);
                
                //Debug.Log("I am attempting to spawn here: " + FoxHelper.print(spawnPoint));
                floorData.setTile(spawnPoint, stencil, spawnLocally, parent);
                
            }
        }
        
        if (voiceControl)
        {
            if (dictScript.lastResult != lastVoiceCommand)
            {
                lastVoiceCommand = dictScript.lastResult;
                Debug.Log("Voice Command Detected: " + dictScript.lastResult);
            }
            
        }
    }

    
}
