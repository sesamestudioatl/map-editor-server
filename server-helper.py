import os

from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/startServer')
def startServer():
    os.system("node server-iso-map-editor.js &")
    os.system("echo 'Server started'")
    return 'Starting World!'

@app.route('/restartServer')
def restartServer():
    os.system("pkill node")
    os.system("echo 'Server killed'")
    os.system("echo 'Server started'")
    os.system("node server-iso-map-editor.js &")
    return 'Restarting World!'

@app.route('/stopServer')
def stopServer():
    os.system("pkill node")
    os.system("echo 'Server killed'")
    return 'Bye Bye World!'

@app.route('/updateServer')
def updateServer():
    os.system("./pull.sh")
    return "Pulled new server code"

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=7777)
