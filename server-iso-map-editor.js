
var port = 3000;
var io = require('socket.io')(process.env.PORT || port);
var shortid = require('shortid');

console.log('Server is running on port ' + port);


io.on('connection', function(socket){
    
    var thisPlayerId = shortid.generate();
    
    console.log('client connected. id:', thisPlayerId);
    
    socket.on('tileSpawn', function(data){
        console.log(data)

        socket.broadcast.emit('tileSpawn', data);
    })
	
    socket.on('disconnect',function () {
		console.log('client disconnected ' + thisPlayerId);
        
        socket.broadcast.emit('disconnected', { id: thisPlayerId });
	});
});
