﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushDict : MonoBehaviour {
    static Dictionary<string, GameObject> brushDict;

    public GameObject[] brushes;

    void Start()
    {
        Debug.Log("Loading stencils");

        brushDict = new Dictionary<string, GameObject>();
        for (int i = 0; i < brushes.Length; i++)
        {
            brushDict[brushes[i].name] = brushes[i];
        }
    }

    public static GameObject get(string prefabName)
    {
        return brushDict[prefabName];
    }
}
