﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Windows.Speech;

public class DictationScript : MonoBehaviour
{
    public string lastResult = "";
    public bool appendToDebugUI = false;

    [SerializeField]
    private Text m_Hypotheses;

    [SerializeField]
    private Text m_Recognitions;

    private DictationRecognizer m_DictationRecognizer;

    void Start()
    {
        m_DictationRecognizer = new DictationRecognizer();

        m_DictationRecognizer.DictationResult += (text, confidence) =>
        {
            lastResult = text;
            if (appendToDebugUI)
            {
                Debug.LogFormat("Dictation result: {0}", text);
                m_Recognitions.text += text + "\n";
            }
                
        };

        m_DictationRecognizer.DictationHypothesis += (text) =>
        {
            
            if (appendToDebugUI)
            {
                Debug.LogFormat("Dictation hypothesis: {0}", text);
                m_Hypotheses.text += text;
            }
                
        };

        m_DictationRecognizer.DictationComplete += (completionCause) =>
        {
            if (completionCause != DictationCompletionCause.Complete)
                Debug.Log("Dictation completed unsuccessfully: {0}." + completionCause.ToString());
        };

        m_DictationRecognizer.DictationError += (error, hresult) =>
        {
            Debug.Log("Dictation error: {0}; HResult = {1}." + error.ToString() + hresult.ToString());
        };

        m_DictationRecognizer.Start();
    }
}